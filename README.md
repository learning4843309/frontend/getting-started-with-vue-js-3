# Vue JS 3: learning path for beginners

https://vueschool.io/learning-paths

## Imported directly in HTML header

https://vueschool.io/lessons/getting-started-with-vue-js-3

Pure HTML and Vue. Nothing to build!

- include Vue using its CDN
- Create a Vue instance and pass empty config for now
- mount to the div ID in HTML
- display data returned as an object by the Vue instance
- in HTML, using the templating `{{ }}` "doubled-mustache" syntax to return the variable from that Vue object
- demonstrate how easy it is to create an input field which modifies live the value passed in the previous object 
- create a variable for the Vue instance to play with it in the console.

## Using Vue Devtools with Vue.js 3

https://vueschool.io/lessons/using-vue-dev-tools-with-vuejs-3


- installing **Vue Devtools** from Chrome/[Firefox](https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)
- 
